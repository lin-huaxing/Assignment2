<?php
namespace App\Models;
use App\Models\Simple\XMLModel;
/*
 * Mock travel destination data.
 * Note that we don't have to extend CodeIgniter's model for now
 */

class Singers extends XMLModel{

    protected $origin = WRITEPATH . 'data/singerData.xml';
    protected $keyField = 'id';
    protected $validationRules = [];

}
