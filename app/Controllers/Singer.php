<?php

namespace App\Controllers;

class Singer extends BaseController {

    public function getIndex() {
        // connect to the model
        $singers = new \App\Models\Singers();
        // retrieve all the records
        $records = $singers->findAll();
        
        $table = new \CodeIgniter\View\Table();
        
        $headings = $singers->fields;
        $displayHeadings = array_slice($headings, 1, 3);
        $table->setHeading(array_map('ucfirst', $displayHeadings));
        
        foreach ($records as $record) {
            $nameLink = anchor("singer/showme/$record->id", 'Details');
            $table->addRow($record->name, "<img height='300' width='250' src='/image/$record->image'/>", $nameLink);
        }
        
        $template = [
            'table_open' => '<table cellpadding="5px">',
            'cell_start' => '<td style="border: 1px solid #dddddd;">',
            'row_alt_start' => '<tr style="background-color:#dddddd">',
        ];
        $table->setTemplate($template);
        
        $fields = [
            'title' => 'Chinese female love song singer',
            'heading' => 'Chinese female love song singer',
            'footer' => 'Copyright LinHuaXing'
        ];
        
        // get a template parser
        $parser = \Config\Services::parser();
        // tell it about the substitions
        return $parser->setData($fields)
                        ->render('templates\top') .
                $table->generate() .
                        $parser->setData($fields)
                        ->render('templates\bottom');
    }

    public function getShowme($id) {
        // connect to the model
        $singers = new \App\Models\Singers();
        // retrieve all the records
        $record = $singers->find($id);
        
        $table = new \CodeIgniter\View\Table();
        
        $table->addRow('Name', $record['name']);
        $table->addRow('Nationality', $record['Nationality']);
        $table->addRow('Nationality2', $record['Nationality2']);
        $table->addRow('Birthplace', $record['Birthplace']);
        $table->addRow('Representative', $record['Representative']);
        $table->addRow('Image', "<img src="."/image/".$record['image']."/>");
        
        $table->addRow(anchor("/","back"));
        
         $fields = [
            'title' => 'Chinese female love song singer',
            'heading' => 'Chinese female love song singer',
            'footer' => 'Copyright LinHuaXing'
        ];
        
         $template = [
            'table_open' => '<table cellpadding="5px">',
            'cell_start' => '<td style="border: 1px solid #dddddd;">',
            'row_alt_start' => '<tr style="background-color:#dddddd">',
        ];
        $table->setTemplate($template);
        
       
        
        // get a template parser
        $parser = \Config\Services::parser();
        // tell it about the substitions
        return $parser->setData($fields)
                        ->render('templates\top') .
                $table->generate() .
                        $parser->setData($fields)
                        ->render('templates\bottom');
    }

}
